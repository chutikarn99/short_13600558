<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('new');
//});



//Auth::routes();

    Route::get('/','TodosController@index'); //ต่อตรง
    Route::get('/t/{short_url}','TodosController@show');
    Route::resource('/','TodosController');


    //Route::get('/home', 'HomeController@index')->name('home');


