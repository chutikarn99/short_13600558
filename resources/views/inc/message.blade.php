
@if(session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{session('success')}}

        <button type="button" class="close" class="btn btn-warning" data-dismiss="alert">
            <span>x</span>
        </button>
    </div>
@endif

