{{--index.blade.php--}}

@extends('layouts.app')
@section('content')
    <h1>List</h1>
    <a href="{{url('/new') }}" > <button type="button" class="btn btn-secondary">new post</button></a>

    @if(count($todos)>0)
        @foreach($todos as $todo)
            <div>
                <p>{{$todo->created_at}}</p>
                <a href="{{url($todo->long_url)}}">
                    <p>{{$todo->long_url}}</p></a>

                {{--<p> View : {{$todo->view}}</p>--}}
                {{--<div class="input-group mb-3">--}}
                    {{--<input id="short_url{{$todo->id}}" class="form-control" type="text" value="http://www.short.local/t/{{$todo->short_url}}" readonly>--}}
                    {{--<div class="input-group-append">--}}


                        {{--<button  onclick=" copy(this)" value="{{$todo->id}}" type="button" class="btn btn-primary">copy</button>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="input-group mb-3" >
                    <div class="input-group-prepend">
                        <span class="input-group-text">View : {{$todo->view}}</span>

                    </div>
                    <input id="short_url{{$todo->id}}" class="form-control" type="text" style="background: #ffffff" value="http://www.short.local/t/{{$todo->short_url}}" readonly>
                    <div class="input-group-append">


                        <button  onclick=" copy(this)" value="{{$todo->id}}" type="button" class="btn btn-warning">Copy me</button>
                    </div>
                </div>






            <hr>
        @endforeach
    @endif
    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#short_url'+id);
            copyText.select();
            document.execCommand('copy');
            alert('copied' + copyText.value);
            
        }
    </script>

@endsection