@extends('layouts.app')
@section('content')
    <h1>New post</h1>
    <a href="{{url('/') }}" > <button type="button" class="btn btn-outline-danger">list</button></a>

    <form method="post" action="{{url('/')}}" >
        @csrf


        <div class="form-group">
            <label>Long url</label>
            <input type="text" name="long_url" class="form-control" >
        </div>


        <button type="submit" class="btn btn-warning" >Create Short URL</button>

    </form>
@endsection
