<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $todos = Todo::OrderBy('created_at','desc')->get();
        return view('index')->with('todos',$todos);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('new');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $charset = "abcdefghijklmnopqrstuvwxyz";
        $numset = "0123456789";

        $url = " ";
        for($i = 0; $i<5; $i++)
        {
            $url .= $numset[rand(0,strlen($numset))-1];
        }
        for($i = 0; $i<3; $i++) {

            $url .= $charset[rand(0, strlen($charset)) - 1];
        }
            $this ->validate($request,
            [
                'long_url' => 'required',
            ]

        );

        $todo = new Todo();
        $todo ->long_url = $request->input('long_url');
        $todo ->short_url = $url;
        $todo ->view = 0;
        $todo->save();

        return redirect('/new')->with('success','Success! http://short.local/t/'.$todo->short_url);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($shorturl)
    {
        //

        $todos = Todo::all();
        if(count($todos)>0){
            foreach ($todos as $todo){
                if ($todo->short_url == $shorturl){
                    $todo->view += 1;
                    $todo->save();
                    return view('redirect')->with('long_url',$todo->long_url);
                }
            }
        }
        return view('notfound');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
